#include "field.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
using namespace std;

/** 
* Construtor da classe 
**/
Field::Field(){
	
	/** Aloca uma matriz do tipo "Field_Game" de tamanho 20x20 onde o jogo irá se desenvolver **/
	
	FieldG = new Field_Game * [20];
	for (int i=0; i<20; i++){
		FieldG[i] = new Field_Game [20];
	}

	/** Aloca um vetor de 40 posições onde serão salvas as coordenadas que contêm campos transportadores para o início do jogo **/
	TransportPoints = new Posicao [40];

}

/** 
* Destrutor da classe 
**/
Field::~Field(){
	for (int i=0; i<20; i++){
		delete FieldG[i];
	}

	delete [] FieldG;

	delete [] TransportPoints;
}

/** 
* Função para gerar valores aleatórios para os campos transportadores 
**/
void Field::GenerateTPoints(){
	srand(time(NULL));
	int i, linha, coluna;

	for (i=0; i<40; i++){

		linha = rand() % 20;
		coluna = rand() % 20;

		/** Caso as posições aleatórias selecionadas sejam linha 19 e coluna 0 (início do jogo)
		 * 	ou linha 0 e coluna 19 (saída do jogo), novos valores deverão ser gerados.
		 *  **/
		while((linha == 19 && coluna == 0) || (linha == 0 && coluna == 19)){
			linha = rand() % 20;
			coluna = rand() % 20;
		}

		TransportPoints[i].i = linha;
		TransportPoints[i].j = coluna;
	}
}

/** 
* Função para setar os campos que serão transportadores 
**/
void Field::FillTheField(){
	int i, j;

	/** Inicialmente, todos os campos da matriz serão preenchidos 
	 * com pontos e com a situção 0 (não são campos transportáveis)
	**/
	for (i=0; i<20; i++){
		for (j=0; j<20; j++){
			FieldG[i][j].data = '.';
			FieldG[i][j].p.i = i;
			FieldG[i][j].p.j = j;
			FieldG[i][j].situation = 0;
		}
	}
	
	/** O campo onde o jogo é iniciado é preenchido com 'o' **/
	FieldG[19][0].data = 'o';

	/** Os campos da matriz gerados aleatoriamente para serem transportadores 
	 * são preenchidos com 'x' e a situação 1 (campos transportáveis) 
	**/
	for (i=0; i<40; i++){
		FieldG[TransportPoints[i].i][TransportPoints[i].j].data = 'x';
		FieldG[TransportPoints[i].i][TransportPoints[i].j].situation = 1;
	}
}

/** 
* Imprime a matriz do jogo (Usada para depuração) 
**/
void Field::PrintField(){
	int i, j;

    for (i=0; i<20; i++){
        for (j=0; j<20; j++){
				cout << FieldG[i][j].data << " ";
            if (j == 19){
                cout << endl;
            }
        }
    }
}

/**
* Função onde o jogo irá se desenvolver.
* @param Stack<Posicao> Pilha do tipo "Posicao" que será usada para armazenar as posições que o jogador passe no labirinto.
* @param Posicao Posição inicial de onde o jogo começa. No caso, i = 0 e j = 19 (Canto inferior esquerdo).
*/
void Field::Play(Stack<Posicao> * s, Posicao posicao){

	/** Enumeração utilizada para definir as posições do sprite do jogador **/	
	enum Direction {Down, Left, Right, Up};
	
	/** Contador para controlar o número de transportadores que o jogador pode cair (< 8) **/
	int cont = 0;

	/** Posição que o sprite do jogador irá ser inicilizado **/
    sf::Vector2i source(1, Down);

	/** Variáveis utilizadas para controlar a velocidade do movimento do sprite do jogador **/
    float frameCounter = 0, switchFrame = 100, frameSpeed = 500;
    sf::Clock clock;

	/** Criação da janela onde o jogo será mostrado. A janela é de tamanho 640x640 e possui o título "Andando no
	 * Escuro" e possui os botões de fechar e minimizar. **/
    sf::RenderWindow window;
    window.create(sf::VideoMode(640, 640), "Andando no Escuro", sf::Style::Titlebar | sf::Style::Close);
    
    /** Criação de uma variável do tipo "Event" para controlar os eventos de entrada na janela do jogo. **/
    sf::Event event;

	/** Variáveis do tipo Texture que irão armazenar as imagens do jogo **/
    sf::Texture pTexture, pTexture2, pTexture3, pTexture4, pTexture5;
    
    /** Sprite do jogador. **/
    if (!pTexture.loadFromFile("../images/Player.png")){
        cout << "Error: Could not load the player image!" << endl;
    }
    
    /** Sprite da saída. **/
    if (!pTexture2.loadFromFile("../images/Out.png")){
		cout << "Error: Could not load the out image!" << endl;
	}
	
	/** Imagem mostrada quando o jogo é ganho. **/
	if (!pTexture3.loadFromFile("../images/Win.png")){
		cout << "Error: Could not load the win image!" << endl;
	}
	
	/** Imagem do campo onde o jogo se desenvolverá. **/
	if (!pTexture4.loadFromFile("../images/Field.png")){
		cout << "Error: Could not load the field image!" << endl;
	}
	
	/** Imagem mostrada quando o jogo é perdido. **/
	if (!pTexture5.loadFromFile("../images/Lose.png")){
		cout << "Error: Could not load the lose image!" << endl;
	}

	/** Variáveis do tipo "Sprite" que serão usadas para carregar as variáveis do tipo "Texture". **/
    sf::Sprite playerImage, outImage, winImage, fieldImage, loseImage;
    /** Imagem do jogador **/
    playerImage.setTexture(pTexture);
    /** Imagem da saída **/
    outImage.setTexture(pTexture2);
    /** Imagem do jogo ganho **/
    winImage.setTexture(pTexture3);
    /** Imagem do campo **/
    fieldImage.setTexture(pTexture4);
    /** Imagem do jogo perdido **/
    loseImage.setTexture(pTexture5);

	/** Posição onde o sprite do jogador estará no início do jogo.
	 * 	Como o sprite tem tamanho 32x32, a posição onde o jogo é iniciado (linha 19, coluna 0) é multiplicada por 32.
	 * 	O x (primeiro parâmetro) representa a coluna e o y (segundo parâmetro) representa a linha. 
	 *  **/
    playerImage.setPosition(0, 608);
    
    /** Posição onde o sprite da saída estará no jogo.
	 * 	Como o sprite tem tamanho 32x32, a posição onde a saída do jogo se encontra (linha 0, coluna 19) é multiplicada por 32.
	 * 	O x (primeiro parâmetro) representa a coluna e o y (segundo parâmetro) representa a linha. 
	 *  **/
    outImage.setPosition(608, 0);
    
	/** Variável do tipo SoundBuffer que irá armazenar o caminho onde o som do campo transportador se encontra. **/
	sf::SoundBuffer b1;
	
	/** Variável do tipo Sound que irá carregar o som anteriormente armazenado em buffer. **/
	sf::Sound teleport;
	
	/** Variável do tipo Music que irá carregar o som que irá tocar durante o jogo. **/
	sf::Music background;
	
	if (!b1.loadFromFile("../sound/teleport.ogg")){
        cout << "Error: Could not load teleport.ogg!" << endl;
	}
	
	if (!background.openFromFile("../sound/background.ogg")){
		cout << "Error: Could not load background.ogg!" << endl;
	}
	
	teleport.setBuffer(b1);
	
	/** Tocar o som de fundo **/
	background.play();
	/** Repetir o som de fundo **/
	background.setLoop(true);
    
    /** Desabilita a possibilidade de repetição de pressionamento de uma mesma tecla **/
    window.setKeyRepeatEnabled(false);

	/** Enquanto a janela do jogo estiver aberta... **/
    while(window.isOpen()){
		
		/** Desenfileira o evento no topo da fila de eventos, se houver, e devolve-o. **/
		while(window.pollEvent(event)){
			
			/** Caso o evento seja o pressionamento do botão de fechar, a janela é fechada e o jogo encerrado. **/
			if (event.type == sf::Event::Closed){
                 window.close();
			}
			 
			/** Caso o evento seja o pressionamento de uma tecla **/ 
			if (event.type == sf::Event::KeyPressed){
				/** Condição para garantir que o sprite está entre os limites da matriz e que o 
				 * contador do número de jogadas é menor do que oito.
				**/
				if ((posicao.i != 0 || posicao.j != 19) && cont < 8){
					/** Pressionamento do tecla de posição Left (esquerda) **/
					if(event.key.code == sf::Keyboard::Left){
						/** A posição do sprite é alterada para a esquerda **/
						source.y = Left;
						
						/** Condição para conferir se o movimento do sprite não irá passar dos limites do jogo. **/
						if (posicao.j != 0 && (playerImage.getPosition().x - 32 >= 0)){
							/** Posição onde o sprite estava na matriz é preenchido com '.' **/
							FieldG[posicao.i][posicao.j].data = '.';
							/** A posição da coluna é decrementada em 1 **/
							posicao.j -= 1;
							/** O sprite se move em uma posição (que equivale a 32 pixels) para a esquerda **/
							playerImage.move(-32, 0);
							/** A última posição armazenada na pilha é removida. **/
							s->pop();
							
							/** Condição para conferir se o campo onde o sprite se moveu é um campo transportador. **/
							if (FieldG[posicao.i][posicao.j].situation == 1){
								/** Em caso afirmativo, o som do transporte é soado. **/
								teleport.play();
								/** O sprite retorna para a posição linha 19, coluna 0. **/
								posicao.i = 19;
								posicao.j = 0;
								playerImage.setPosition(0, 608);
								/** A pilha é esvaziada. **/
								s->clear();
								/** O contador do número de jogadas é incrementado. **/
								cont++;
							}
							/** A nova posição do sprite é preenchida com 'o' **/
							FieldG[posicao.i][posicao.j].data = 'o';
						}
					 }
					 
					 /** Pressionamento do tecla de posição Right (direita) **/
					 if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
						
						/** A posição do sprite é alterada para a direita. **/
						source.y = Right;
	
						/** Condição para conferir se o movimento do sprite não irá passar dos limites do jogo. **/
						if (posicao.j != 19 && (playerImage.getPosition().x + 32) <= 608){
							/** Posição onde o sprite estava na matriz é preenchido com '.' **/
							FieldG[posicao.i][posicao.j].data = '.';
							/** A posição da coluna é incrementada em 1 **/
							posicao.j += 1;
							/** O sprite se move em uma posição (que equivale a 32 pixels) para a direita **/
							playerImage.move(32, 0);
							
							/** Condição para conferir se o campo onde o sprite se moveu é um campo transportador. **/
							if (FieldG[posicao.i][posicao.j].situation == 1){
								/** Em caso afirmativo, o som do transporte é soado. **/
								teleport.play();
								/** O sprite retorna para a posição linha 19, coluna 0. **/
								posicao.i = 19;
								posicao.j = 0;
								playerImage.setPosition(0, 608);
								/** A pilha é esvaziada. **/
								s->clear();
								/** O contador do número de jogadas é incrementado. **/
								cont++;
							}
							
							/** A nova posição do sprite é armazenada na pilha. **/
							s->push(posicao);
							/** A nova posição do sprite é preenchida com 'o' **/
							FieldG[posicao.i][posicao.j].data = 'o';
						}
					 }
					 
					 /** Pressionamento do tecla de posição Up (para cima) **/
					 if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
						 
						 /** A posição do sprite é alterada para cima. **/
						 source.y = Up;
						
						/** Condição para conferir se o movimento do sprite não irá passar dos limites do jogo. **/
						if (posicao.i != 0 && (playerImage.getPosition().y - 32) >= 0){
							/** Posição onde o sprite estava na matriz é preenchido com '.' **/
							FieldG[posicao.i][posicao.j].data = '.';
							/** A posição da linha é decrementada em 1 **/
							posicao.i -= 1;
							/** O sprite se move em uma posição (que equivale a 32 pixels) para cima **/
							playerImage.move(0, -32);
							
							/** Condição para conferir se o campo onde o sprite se moveu é um campo transportador. **/
							if (FieldG[posicao.i][posicao.j].situation == 1){
								/** Em caso afirmativo, o som do transporte é soado. **/
								teleport.play();
								/** O sprite retorna para a posição linha 19, coluna 0. **/
								posicao.i = 19;
								posicao.j = 0;
								playerImage.setPosition(0, 608);
								/** A pilha é esvaziada. **/
								s->clear();
								/** O contador do número de jogadas é incrementado. **/
								cont++;
							}
							
							/** A nova posição do sprite é armazenada na pilha. **/
							s->push(posicao);
							/** A nova posição do sprite é preenchida com 'o' **/
							FieldG[posicao.i][posicao.j].data = 'o';
						}
					 }
					 
					 /** Pressionamento do tecla de posição Down (para baixo) **/
					 if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
						
						/** A posição do sprite é alterada para baixo. **/
						source.y = Down;
						
						/** Condição para conferir se o movimento do sprite não irá passar dos limites do jogo. **/
						if (posicao.i != 19 && (playerImage.getPosition().y + 32) <= 608){
							/** Posição onde o sprite estava na matriz é preenchido com '.' **/
							FieldG[posicao.i][posicao.j].data = '.';
							/** A posição da linha é incrementada em 1 **/
							posicao.i += 1;
							/** O sprite se move em uma posição (que equivale a 32 pixels) para baixo **/
							playerImage.move(0, 32);
							/** A última posição armazenada na pilha é removida. **/
							s->pop();
							if (FieldG[posicao.i][posicao.j].situation == 1){
								/** Em caso afirmativo, o som do transporte é soado. **/
								teleport.play();
								/** O sprite retorna para a posição linha 19, coluna 0. **/
								posicao.i = 19;
								posicao.j = 0;
								playerImage.setPosition(0, 608);
								/** A pilha é esvaziada. **/
								s->clear();
								/** O contador do número de jogadas é incrementado. **/
								cont++;
							}
							/** A nova posição do sprite é preenchida com 'o' **/
							FieldG[posicao.i][posicao.j].data = 'o';
						}
					 }
					
					/** Opções para controlar a velocidade da movimentação do sprite no jogo. **/
					frameCounter += frameSpeed * clock.restart().asSeconds();
					if (frameCounter >= switchFrame){
						frameCounter = 0;
						source.x++;
						if (source.x * 32 >= pTexture.getSize().x){
							source.x = 0;
						}
					}
				}
			}
			
			/** Operação para selecionar o sprite certo do jogador que deve ser exibido. **/
			playerImage.setTextureRect(sf::IntRect(source.x * 32, source.y * 32, 32, 32));
		}
		
		/** Desenha na janela o campo do jogo. **/
		window.draw(fieldImage);
		
		/** Desenha na janela o sprite do jogador. **/
		window.draw(playerImage);
		
		/** Desenha na janela a imagem da saída do jogo. **/
		window.draw(outImage);
		
		/** Condição de fim de jogo ganho. **/
		if (posicao.i == 0 && posicao.j == 19 && cont < 8){
			
			/** A janela é limpada. **/
			window.clear();
			
			/** A imagem de jogo ganho é mostrada. **/
			winImage.setPosition(0, 220);
			window.draw(winImage);
			
			/** A música de fundo para de tocar. **/
			background.stop();
			
			/** Pressionando a tecla "Space" o jogo é encerrado. **/
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				break;
			}
		/** Condição de fim de jogo perdido. **/
		}else if (cont == 8){
			
			/** A janela é limpada. **/
			window.clear();
			
			/** A imagem de jogo perdido é mostrada. **/
			loseImage.setPosition(0, 220);
			window.draw(loseImage);
			
			/** A música de fundo para de tocar. **/
			background.stop();
			
			/** Pressionando a tecla "Space" o jogo é encerrado. **/
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				break;
			}
		}
		
		/** A janela é mostrada. **/
		window.display();
		
		/** A janela é limpada. **/
		window.clear();
    }
}
