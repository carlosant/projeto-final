/** Estrutura utilizada para armazenar as coordenadas dos objetos na matriz do jogo **/
struct Posicao{
		int i;	/** Posição da linha (Entre 0 e 19) **/
		int j;	/** Posição da coluna (Entre 0 e 19) **/
};

/** Estrutura usada para simular um campo onde o jogo se desenvolve **/
struct Field_Game{
		char data;	 	/** Dado armazenado em um campo da matriz ('.' - campo comum, 'x' - campo transportador, 'o' - posição do jogador **/
		Posicao p;		/** Coordenadas linha e coluna de cada campo da matriz **/
		bool situation;	/** Situação do campo: 0 - Campo Comum, 1 - Campo Transportador **/
};

	
