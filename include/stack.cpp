#include "stack.h"
#include "doubleArray.cpp"
#include <iostream>
#include <stdexcept>

/**
* Construtor da classe pilha.
*/
template <class Object>
Stack<Object>::Stack( int _iSz ) :
    mi_Top( -1 ),
    mi_Capacity( _iSz ),
    mp_Data( NULL )
{
    try {  mp_Data = new Object[ mi_Capacity ];  }
    catch ( std::bad_alloc& e ) {
        std::cerr << "  >>> [Stack<T>::Stack()] Dynamic memory allocation failed!\n";
        throw e;
    }
}

/**
* Destrutor da classe pilha.
*/
template <class Object>
Stack<Object>::~Stack( )
{
    delete [] mp_Data;
}

/**
* Testa se a pilha está vazia.
* @return "True" caso esteja vazia e "False" caso contrário.
*/
template <class Object>
bool Stack<Object>::empty( ) const
{
    return ( mi_Top == -1 );
}

/**
* Faz a pilha ficar logicamente vazia ao manipular seu ponteiro 'mi_Top'.
*/
template <class Object>
void Stack<Object>::clear( )
{
    mi_Top = -1;
}

/**
* Método que retorna o elemento do topo da pilha, ou seja, o item mais recente.
* @return Objeto mais recente da pilha.
*/
template <class Object>
const Object & Stack<Object>::top( ) const
{
    if( empty( ) )
        throw std::underflow_error("  >>> [Stack<T>::top()] Underflow detected!\n");
    return mp_Data[ mi_Top ];
}

/**
* Método que retira o objeto mais recente da pilha e o retorna.
* @return Objeto retirado da pilha.
*/
template <class Object>
const Object & Stack<Object>::pop( )
{
    if( empty( ) )
        throw std::underflow_error("  >>> [Stack<T>::pop()] Underflow detected!\n");
    return mp_Data[ mi_Top-- ];
}

/**
* Método que adiciona um objeto na pilha.
* @param x Objeto a ser inserido na pilha.
*/
template <class Object>
void Stack<Object>::push( const Object & _x )
{
    if( mi_Top == mi_Capacity -1 )
    {
        DoubleArray( mp_Data, mi_Capacity );
    }
    mp_Data[ ++mi_Top ] = _x;
}
