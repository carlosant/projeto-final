#ifndef _STACK_
#define _STACK_

#include "estruturas.cpp"

/**
 * Classe que constr�i e manipula uma estrutura de dados do tipo pilha com suporte a templates.
*/
template <class Object>
class Stack{
  public:
	/**
	 * Construtor da classe pilha com valor padr�o 20.
	*/
    explicit Stack( int _iSz = 20 );
    /**
     * Destrutor da classe pilha.
    */
    ~Stack();

	/**
	 * M�todo que adiciona um objeto na pilha.
	 * @param x Objeto a ser inserido na pilha.
	*/
    void push( const Object & x );

    /**
     * M�todo que retira o objeto mais recente da pilha e o retorna.
     * @return Objeto retirado da pilha.
    */
    const Object & pop( );

    /**
     * M�todo que retorna o elemento do topo da pilha, ou seja, o item mais recente.
     * @return Objeto mais recente da pilha.
    */
    const Object & top( ) const;

    /**
     * Testa se a pilha est� vazia.
     * @return "True" caso esteja vazia e "False" caso contr�rio.
    */
    bool empty( ) const;

    /**
     * Faz a pilha ficar logicamente vazia ao manipular seus ponteiros.
    */
    void clear( );

  private:
    int mi_Top; /** Ponteiro para o topo da pilha */
    int mi_Capacity; /** Capacidade de armazenamento da pilha */
    Object *mp_Data; /** Vetor para armazenar os valores da pilha */
};

#include "stack.cpp"

#endif
