#ifndef _FIELD_
#define _FIELD_

#include "stack.h"

/** Classe responsável por criar o campo onde o jogo se desenvolverá. **/
class Field{

  public:
	/** 
	 * Construtor da classe 
	**/
	Field();

	/** 
	 * Destrutor da classe 
	**/
	~Field();

	/** 
	 * Função para gerar valores aleatórios para os campos transportadores 
	**/
	void GenerateTPoints();

	/** 
	 * Função para setar os campos que serão transportadores 
	**/
	void FillTheField();

	/** 
	 * Imprime a matriz do jogo (Usada para depuração) 
	**/
	void PrintField();

	/**
	 *  Função onde o jogo irá se desenvolver 
	 * 	@param Stack<Posicao> Pilha do tipo "Posicao" que será usada para armazenar as posições que o jogador passe no labirinto.
	 * 	@param Posicao Posição inicial de onde o jogo começa. No caso, i = 0 e j = 19 (Canto inferior esquerdo).
	**/
	void Play(Stack<Posicao> * s, Posicao posicao);

  private:
	Field_Game **FieldG;		/** Matriz do tipo FieldG **/
	Posicao *TransportPoints;	/** Vetor contendo as posições dos pontos de transporte **/ 
};

#include "field.cpp"

#endif
