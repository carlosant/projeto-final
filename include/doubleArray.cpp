#include <iostream>
using namespace std;
#include <stdlib.h>
#include <new>
using std::bad_alloc;
#include <cassert>

/** Função utilizada para duplicar a capacidade de armazenamento da pilha **/
template <typename T>
void DoubleArray ( T * & A, int &currentSize){
	
	/** Aloca um vetor auxiliar com o dobro do tamanho original **/
	T *aux = new T [2*currentSize];
	
	/**Copiar os valores do vetor original para o novo vetor auxiliar **/
	for (int i(0); i<currentSize; ++i){
		aux[i] = A[i];
	}
	
	/** Deleta o vetor original **/
	delete [] A;
	
	/** Faz o ponteiro do vetor original apontar para a nova memória (vetor auxiliar) **/
	A = aux;
	
	/** Atualizar o tamanho do vetor **/
	currentSize *= 2;
}
