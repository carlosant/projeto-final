PROJETO FINAL
Andando no Escuro
====


### Aluno: Carlos Antônio de Oliveira Neto

### e-mail: carlosantonio.o.n@outlook.com

### Turma: 01

** Compilação: ** 

g++ -c main.cpp

g++ main.o -o saida -lsfml-window -lsfml-graphics -lsfml-system -lsfml-audio

** Estrutura de dados utilizada: ** 

Pilha

** Explicações do jogo: ** 

O jogo "Andando no Escuro" é, basicamente, um jogo que usa conceitos de jogos de labirinto e memória.
O jogo desenvolve-se em uma matriz de tamanho 20x20 onde, entre os seus campos, há 20 deles  que, se pisados,
fazem o jogador retornar para a posição inicial (inferior esquerda) da matriz.

Para vencê-lo, o jogador deve ir até a saída (campo superior direito) com cuidado para que não pise em 8 posições
que o façam retornar para o início do jogo (ao pisar 8 vezes, o jogo é encerrado e o jogador perde).

A estrutura de dados pilha foi utilizada na depuração do caminho que o jogador percorre na matriz. A cada
movimento para cima ou para a direita a posição que o jogador se movimentará é armazenada na pilha, ao passo de que
cada movimento para baixo ou para a esquerda, a última posição armazenada é retirada da pilha.
Caso o jogador volte para o início do jogo, toda a pilha é desempihada.