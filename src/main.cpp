#include "../include/field.h"
#include "../include/stack.h"
#include <SFML/Graphics.hpp>
#include <iostream>
using namespace std;

int main (void){

	/** Campo onde o jogo se desenvolverá **/
	Field A;
	
	/** Pilha para armazenar as posições que o jogador passa durante o jogo **/
	Stack<Posicao> P;
	
	/** Posição onde o jogo iniciará (linha 19, coluna 0 = canto inferior esquerdo) **/
	Posicao posicao;

	posicao.i = 19;
	posicao.j = 0;

	A.GenerateTPoints();

	A.FillTheField();

	A.Play(&P, posicao);

	return 0;
}
